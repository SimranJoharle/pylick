#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import re
import glob
#import subprocess
try:
    from setuptools import setup
    setup
except ImportError:
    from distutils.core import setup
    setup


vers = "0.2.0"
githash = ""
with open('pylick/_version.py', "w") as f:
    f.write('__version__ = "{}"\n'.format(vers))
    f.write('__githash__ = "{}"\n'.format(githash))

setup(
    name="pylick",
    url="https://github.com/mmoresco/pylick",
    version=vers,
    author="M. Moresco, N. Borghi, S. Quai",
    author_email="",
    packages=["pylick",
            #   "pylick.indices",
            #   "pylick.measurements",
            #   "pylick.analysis",
            #   "pylick.plot"
              ],

    license="LICENSE",
    description="Stellar Population Inference",
    long_description=open("README.md").read(),
    package_data={"": ["README.md", "LICENSE"]},
    scripts=glob.glob("examples/*.py"),
    include_package_data=True,
    install_requires=["numpy", "scipy", "matplotlib", "astropy"],
)
