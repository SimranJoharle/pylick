
import numpy as np
import pylick.pylick as plk
from astropy.io import fits

import LickMC.IO as io

def load_spectrum(folder_spec, ID, z=None, spec_type='fits'):
    # def detect_spectrum_window(wave,flux):
    #     """ Find the spectrum window excluding non positive side regions
    #     Input: 
    #         - flux 
    #     Output:
    #         - boolean mask with flux > 0 regions
    #     """
    #     il   = 0
    #     ir   = -1
    #     while True:
    #         if (flux[il] > 0):
    #             break
    #         il += 1
    #     while True:
    #         if (flux[ir] > 0):
    #             break
    #         ir -= 1        
    #     flag_window = np.ones_like(wave, dtype=bool)
    #     flag_window[:il] = 0
    #     flag_window[ir:] = 0
    #     return flag_window
        
    if spec_type == 'fits':
        file_spec = 'legac_'+ID.replace(" ", "")+'_v2.0.fits'
        print(file_spec)
        hdulist = fits.open(folder_spec+file_spec)
        values = hdulist[1].data
        hdulist.close()
    wave = values['WAVE'].flatten()
    flux = values['FLUX'].flatten() 
    ferr = values['ERR'].flatten()  
    qual = np.logical_not(values['QUAL'].flatten().astype(bool))  

    qual = qual & (flux>0) & (ferr>0)

    ferr[~qual] = 9e99
    print(np.sum(ferr==0))

    if z is not None:
        wave = wave / (1+z)
        window     = detect_spectrum_window(wave, flux)
        wave       = wave[window]
        flux       = flux[window]
        ferr       = ferr[window]
        qual       = qual[window]

    return wave,flux,ferr,qual



def load_spec(ID):
    def detect_spectrum_window(flux):
        """ Find the spectrum window excluding non positive side regions
        Input: 
            - flux 
        Output:
            - boolean mask with flux > 0 regions
        """
        il   = 0
        ir   = -1
        while True:
            if (flux[il] > 0):
                break
            il += 1
        while True:
            if (flux[ir] > 0):
                break
            ir -= 1        
        flag_window = np.ones_like(flux, dtype=bool)
        flag_window[:il] = 0
        flag_window[ir:] = 0
        return flag_window
    
    """ Loads spectroscopic data from file. """
    dir_spec = '/home/nic/tesi/CC_LEGAC/1_Catalog/legac_DR2_spectra/'
    hdulist  = fits.open(dir_spec+'legac_'+ID.replace(" ", "")+'_v2.0.fits')

    wave = hdulist[1].data["WAVE"][0]
    flux = hdulist[1].data["FLUX"][0]
    ferr = hdulist[1].data["ERR"][0]
    qual = hdulist[1].data["QUAL"][0]

    mask = detect_spectrum_window(flux)
    
    spectrum = [wave[mask], 
                flux[mask], 
                ferr[mask],
                qual[mask]]
                
    hdulist.close()

    return spectrum



bools = [-1]
bools = [-2,-1]

IDs = io.load_catalog(select='passive',cols='SPECT_ID')#[bools]
z = io.load_catalog(select='passive',cols='z')#[bools]


# B et al.
index_list = np.arange(22, 46) # 24
# index_list = [25]


settings={}
# settings['plt_outformat'] = '.pdf'
# settings['plt_domask']    = True
# settings['plt_undone_idx']= True
# settings['plt_inspect']   = True

arr = np.array([(1, 2.0, 'x'),
                (4, 5.0, 'y')],
                dtype=[('a', 'i4'), ('b', 'f8'), ('c', 'S2')])


plk.pylick(IDs, load_spec, index_list=index_list, meas_method='int', z=z, 
        do_plot=False, index_table=None, pltsettings=settings, verbose=False)




# os.system('pdfunite $(ls -tr emcee*.pdf) Binder.pdf')



