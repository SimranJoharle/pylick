import os
import os.path
from termcolor import colored
import timeit

#Import PyLick
from PyLick.IO import *
from PyLick.Measurements import *
from PyLick.Measurements_interpolate import *

os.system('clear')
print(colored("#######################################", 'red'))
print(colored("### Program to measure Lick indices ###", 'red'))
print(colored("#######################################", 'red'))
print(colored("#### authors: M. Moresco, S. Quai #####", 'red'))
print(colored("#######################################", 'red'))

# Measurements method: 'interp' or 'wei'
meas_method='interp'

# Define directories and outputs
dir_home = os.getcwd()
dir_input = dir_home+'/input/'
dir_output = dir_home+'/output/'
dir_plot = dir_home+'/plots/'
fileLick='Lick_table_v3.0.dat'
fileOUT_val="output_val_"+meas_method+".dat"
fileOUT_err="output_err_"+meas_method+".dat"

# Output to screen/file
write_output_screen=1
write_plots=1

# Checks --> compare interpolation and weighting
check_method=1

# Read spectra ID
os.chdir(dir_input)
fileID='list_test.dat'
ID = np.genfromtxt(fileID, comments="#", usecols=(0), dtype=str, unpack=True)
redshift=np.empty(len(ID))
#redshift = np.genfromtxt(fileID, comments="#", usecols=(1), unpack=True)

os.chdir(dir_home)


start_time = timeit.default_timer()

# Read indices to be measured
cbp_b,cbp_r,fbp_bb,fbp_br,fbp_rb,fbp_rr,unit,ind=read_indices(fileLick,dir_input,dir_home)


for j in range(len(ID)):
	# Read spectrum: ASCII (already restframe), FITS (restframe conversion)
	# BOSS read has AIR-VACUUM conversion
	#llambda,flux,noise,redshift[j]=read_spec_ASCII_BOSS(ID[j],dir_input,dir_home)
	#llambda,flux,noise,redshift[j]=read_spec_FITS_BOSS(ID[j],dir_input,dir_home)
	#llambda,flux,noise,redshift[j]=read_spec_FITS(ID[j],dir_input,dir_home)
	llambda,flux,noise, redshift[j]=read_spec_FITS(ID[j],dir_input,dir_home)
	
	# Average dispersion of the spectrum
	# SPECTRA SHOULD BE LINEARLY SAMPLED, so disp=pixel dimension
	dispv=np.empty(len(llambda))
	for kk in range(len(llambda)-1):
 		dispv[kk] = llambda[kk+1]-llambda[kk]
	disp=dispv.mean()
	disp_err=dispv.std()
	#print colored('pixel size = ', 'blue'), disp, " \pm ", disp_err, "\n"
	
	# Define vectors
	old_lick_value=np.empty(len(ind))
	old_lick_error=np.empty(len(ind))
	lick_value=np.empty(len(ind))
	lick_error=np.empty(len(ind))

	if (j==0):	
		# Open output file
		fout_val,fout_err=open_output(dir_output,fileOUT_val,fileOUT_err,ind)

	if (meas_method=='interp') or (meas_method=='wei' and check_method==1):	
		# Interpolate spectrum (0.0625 AA)
		# NEEDED for method INTERPOLATE
		bin=0.0625
		lambda_intp,flux_intp=flux_interp(llambda,flux,bin)


	if (meas_method=='wei'):
		# Measure indices	
		for i in range(len(ind)):
			if (unit[i]=='A' or unit[i] == 'mag') :
				lick_value[i],lick_error[i]=measure_lick_ind_wei(llambda,flux,noise,fbp_bb[i],fbp_br[i],cbp_b[i],cbp_r[i],fbp_rb[i],fbp_rr[i],unit[i])
				if(check_method==1):
					old_lick_value[i]=measure_lick_ind_int(lambda_intp,flux_intp,fbp_bb[i],fbp_br[i],cbp_b[i],cbp_r[i],fbp_rb[i],fbp_rr[i],unit[i])
					old_lick_error[i]=measure_lick_err_int(llambda,flux,noise,disp,lick_value[i],fbp_bb[i],fbp_br[i],cbp_b[i],cbp_r[i],fbp_rb[i],fbp_rr[i],unit[i])
			elif (unit[i] == 'break_nu') :
				lick_value[i],lick_error[i]=measure_break_nu_wei(llambda,flux,noise,fbp_bb[i],fbp_br[i],fbp_rb[i],fbp_rr[i])
				if(check_method==1):
					old_lick_value[i],old_lick_error[i]=measure_break_nu_int(llambda,flux,noise,disp,fbp_bb[i],fbp_br[i],fbp_rb[i],fbp_rr[i])		
			elif (unit[i] == 'break_lb') :
				lick_value[i],lick_error[i]=measure_break_lb_wei(llambda,flux,noise,fbp_bb[i],fbp_br[i],fbp_rb[i],fbp_rr[i])
				if(check_method==1):
					old_lick_value[i],old_lick_error[i]=measure_break_lb_int(llambda,flux,noise,disp,fbp_bb[i],fbp_br[i],fbp_rb[i],fbp_rr[i])		
			elif (unit[i] == 'general') :
				lick_value[i],lick_error[i]=measure_ind_general_wei(llambda,flux,noise,disp)	
				if(check_method==1):
					old_lick_value[i],old_lick_error[i]=measure_ind_general_int(llambda,flux,noise,disp)
			if(write_output_screen==1):
				if (i==0):
					print(colored('Measurements:', 'blue'))
				if(check_method==1):
					print(colored("Weighted:", 'green'), ind[i], "=", lick_value[i], "\pm", lick_error[i])	
					print(colored("Interpolated:", 'red'), ind[i], "=", old_lick_value[i], "\pm", old_lick_error[i])	
				if(check_method!=1):
					print(ind[i], "=", lick_value[i], "\pm", lick_error[i])

	if (meas_method=='interp'):
		# Measure indices	
		for i in range(len(ind)):
			if (unit[i]=='A' or unit[i] == 'mag') :
				lick_value[i]=measure_lick_ind_int(lambda_intp,flux_intp,fbp_bb[i],fbp_br[i],cbp_b[i],cbp_r[i],fbp_rb[i],fbp_rr[i],unit[i])
				lick_error[i]=measure_lick_err_int(llambda,flux,noise,disp,lick_value[i],fbp_bb[i],fbp_br[i],cbp_b[i],cbp_r[i],fbp_rb[i],fbp_rr[i],unit[i])
				if(check_method==1):
					old_lick_value[i],lick_error[i]=measure_lick_ind_wei(llambda,flux,noise,fbp_bb[i],fbp_br[i],cbp_b[i],cbp_r[i],fbp_rb[i],fbp_rr[i],unit[i])
			elif (unit[i] == 'break_nu') :
				lick_value[i],old_lick_error[i]=measure_break_nu_int(llambda,flux,noise,disp,fbp_bb[i],fbp_br[i],fbp_rb[i],fbp_rr[i])		
				if(check_method==1):
					old_lick_value[i],lick_error[i]=measure_break_nu_wei(llambda,flux,noise,fbp_bb[i],fbp_br[i],fbp_rb[i],fbp_rr[i])
			elif (unit[i] == 'break_lb') :
				lick_value[i],old_lick_error[i]=measure_break_lb_int(llambda,flux,noise,disp,fbp_bb[i],fbp_br[i],fbp_rb[i],fbp_rr[i])
				if(check_method==1):
					old_lick_value[i],lick_error[i]=measure_break_lb_wei(llambda,flux,noise,fbp_bb[i],fbp_br[i],fbp_rb[i],fbp_rr[i])
			elif (unit[i] == 'general') :
				lick_value[i],old_lick_error[i]=measure_ind_general_int(llambda,flux,noise,disp)	
				if(check_method==1):
					old_lick_value[i],lick_error[i]=measure_ind_general_wei(llambda,flux,noise,disp)
			if(write_output_screen==1):
				if (i==0):
					print(colored('Measurements:', 'blue'))
				if(check_method==1):
					print(colored("Interpolated:", 'green'), ind[i], "=", lick_value[i], "\pm", lick_error[i])
					print(colored("Weighted:", 'red'), ind[i], "=", old_lick_value[i], "\pm", old_lick_error[i])	
				if(check_method!=1):
					print(ind[i], "=", lick_value[i], "\pm", lick_error[i])
	# Plot index to file
	if (write_plots==1):
		plot_to_file(ID[j],llambda,flux,fbp_bb,fbp_br,cbp_b,cbp_r,fbp_rb,fbp_rr,unit,ind,dir_plot,dir_home)
		
	# Print index to file
	write_output(dir_output,fout_val,fout_err,ID[j],redshift[j],lick_value,lick_error)

fout_val.close()
fout_err.close()


print(colored("\n##############################", 'red'))
print(colored("Total time to analyze ", 'red'), colored(len(ID), 'red'), colored(" spectra=", 'red'), timeit.default_timer()-start_time, " sec")
print(colored("Average time per spectra= ", 'red'), (timeit.default_timer()-start_time)/len(ID), " sec")
print(colored("##############################", 'red'))



