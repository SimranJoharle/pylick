import os
import inspect

# directories
__ROOT__ = '/'.join(os.path.abspath(inspect.getfile(inspect.currentframe())).split('/')[:-1])

dir_lib  = __ROOT__ + '/libs/'

__table__ = dir_lib + 'table.dat'
__style__ = dir_lib + 'plotbelli.style'

__dirRes__  = '../outputs/'
__dirPlot__ = '../outputs/plots/'
