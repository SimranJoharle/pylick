# PyLick
*A Python tool to measure spectral line indices on galaxy spectra*

<img align="right" src="examples/outputs/example-lick.png">

+ Version:
   + **0.2**
+ Dependencies:
   + **Python 3**
   + **`numpy`**
   + **`scipy`**
   + **`matplotlib`**
   + **`astropy`**
+ Acknowledgments:
  + If you use **pyLick**, please reference the code paper **Towards a Better Understanding of Cosmic Chronometers: Stellar Population Properties of Passive Galaxies at Intermediate Redshift** (<a href="https://arxiv.org/abs/2106.14894">Borghi et al. 2021</a>)

## The team
+ Main developers:
  + Michele Moresco (michele.moresco@unibo.it)
  + Nicola Borghi (nicola.borghi6@unibo.it)
  + Salvatore Quai (squai@uvic.ca)
+ Contributors:
  + Alexandre Huchet
  + Lucia Pozzetti (lucia.pozzetti@inaf.it)
  + Andrea Cimatti (a.cimatti@unibo.it)

## Installation
From source:

```
<!-- git clone https://github.com/mmoresco/pylick -->
python -m pip install -U pip
python -m pip install -U setuptools setuptools_scm pep517
git clone https://gitlab.com/mmoresco/pylick.git
cd pylick
python -m pip install -e .

```
environmental variables are pre-set in the code tied to the running directory. Alternatively, they can be set as follows:

```
export PYTHONPATH=$PYTHONPATH:<path_to_pylick_dir>/
```

Using pip:

```
pip install pylick
```

## Run the code
```python
from pylick.analysis import Galaxy, Catalog
```

* **Single Galaxy analysis:** 

      ind = Galaxy(ID, index_list, spec_wave, spec_flux, z=z)

      print(ind.vals)
      print(ind.errs)

    |    parameter    |           type           | description                                                           |
    |:---------------:|:------------------------:|-----------------------------------------------------------------------|
    |       `ID`      |       str, str list      | galaxy unique identifier                                              |
    |   `index_list`  |         int list         | list of indices to measure (see [later](#index_list))                 |
    |   `spec_wave`   |       numpy.ndarray      | wavelength sampling of the spectrum                                   |
    |   `spec_flux`   |       numpy.ndarray      | spectral fluxes corresponding to `spec_wave`                          |
    |    `spec_err`   | numpy.ndarray (optional) | spectral fluxes uncertainties                                         |
    |   `spec_mask`   | numpy.ndarray (optional) | boolean array flagging pixels to interpolate over                     |
    |       `z`       |     float (optional)     | if given, `spec_wave` is computed rest-frame                          |
    |  `meas_method`  |      str (optional)      | method to measure the indices (see [later](#meas_method))             |
    |   `bpr_thres`   |   float (default=0.15)   | bad-to-total pixel ratio above which the measurement is not performed |
    |  `index_table`  |      str (optional)      | path to the user-defined table of indices                             |
    |      `plot`     |   bool (default=False)   | produce plot file                                                     |
    | `plot_settings` |      dict (optional)     | instruction for the plot code (see [later](#meas_method))             |

* **Full catalog analysis:**

      def load_spec(ID):
        ...
        return [wave, flux, ferr, qual]

      IDs = [...]
      index_list = [...]

      Catalog(IDs, load_spec, index_list=index_list, z=zs, do_plot=False, verbose=True)

    |    parameter    |           type           | description                                                                                                                   |
    |:---------------:|:------------------------:|-------------------------------------------------------------------------------------------------------------------------------|
    |      `IDs`      |         str list         | galaxy unique identifiers                                                                                                     |
    | `load_spectrum` |         function         | function that takes one `ID` as argument and returns the corresponding spectral [wave, flux, err (optional), mask (optional)] |
    |  `res_prepend`  | astropy.table (optional) | astropy table to which pre-pend the results                                                                                   |
    |  `res_prename`  |    str list (optional)   | colum names for `res_prepend`                                                                                                 |
    |   `res_format`  |      str (optional)      | format of the astropy table (e.g. 'ascii.ecsv', 'fits', ...)                                                                  |
    |    `res_dir`    |      str (optional)      | directory to save the results table                                                                                           |
    |    `verbose`    |   bool (default=False)   | more information on the measurements                                                                                          |

Some working examples can be found in the `/examples/` sub-folder.

### index_list
List of the IDs of the indices to measure (from the `/data/index_table.dat' table). To see the available ones:
```python
from pylick.indices import IndexLibrary
lib = IndexLibrary()
print(lib)
```
The user can build a table with custom indices definitions and import it with the `index_table` argument.


### meas_method
The pixels of observed spectra can be only partially contained in the pass-bands windows. To account for this effect multiple methods can be used:
- **int (default)**: the spectrum is interpolated with a fine bin size of 1\AA;
- **exact**: the spectrum is interpolated with scipy.interpolate.interp1d (0th order). The resulting function can be evaluated at each wavelenght, therefore the measurement is more precise at the edges of pass-bands windows;
- **wei**: the flux of pixels is weighted by the fraction covered by the pass-bands definition.

**performances (*)**
- **int**: 0.005 s/index/galaxy
- **wei**: 0.010 s/index/galaxy
- **exact**: 0.125 s/index/galaxy (bottleneck: scipy.interpolate on the whole spectrum)

(*) Rough estimates for a 3 GHz processor

### plot_settings
An example of customly defined plot settings (beta)
```python

plot_settings = {'figsize' : (8,4.8),
                 'xlab' : r'Restframe wavelength [$\AA$]',
                 'ylab' : r'Flux [unitless]',
                 'title' : ID,
                 'spec_colors' : ['navy', 'deepskyblue', (.4,.4,.4,1), (.4,.4,.4,1)],
                 'inspect' : False,
                 'outpath': '.',
                 'format' : 'pdf'
                 }
         
```



## Content
Below is a brief description of the contents of the directories in the PyLick root directory:
- **pylick**: the PyLick code and auxilliary files.
- **data**: contains input spectra. The files in this directory are readily user editable.
- **examples**: some .ipynb and example scripts.
- **outputs**: contains the outputs (index values and errors, plot).


